// i used bumplan open gitlab repo as a reference

import { green, yellow, red, blue } from "colorette";

let colorMap = {
  W: green,
  A: red,
  M: yellow,
};

function buildAirplaneSeatLayout(airplaneSeatConfig) {
  let seats = {};

  // extract the rows
  let rows = airplaneSeatConfig.map((config) => config[1]);

  //  iterate over the rows
  let rowsLength = rows?.length;

  let configLength = airplaneSeatConfig?.length;

  for (let rowIndex = 0; rowIndex < rowsLength; rowIndex++) {
    // grab the airplane seat config and iterate over that ... we can access all the arrays
    for (let colIndex = 0; colIndex < configLength; colIndex++) {
      // check if rowIndex under the constratin of the row number of that array
      if (rowIndex < airplaneSeatConfig[colIndex][1]) {
        // grab the subarray
        const columnSeatMap = airplaneSeatConfig[colIndex];

        // grab the specfic array column
        const columns = columnSeatMap[0];

        const colLength = columns;

        // iterate over the columns
        for (
          let innerColIndex = 0;
          innerColIndex < colLength;
          innerColIndex++
        ) {
          let airplaneSeatType;

          // if it the first array
          if (colIndex == 0) {
            // if it the first column

            if (innerColIndex === 0) {
              airplaneSeatType = "W";
            } else if (innerColIndex === colLength - 1) {
              airplaneSeatType = "A";
            } else {
              airplaneSeatType = "M";
            }
          } else if (colIndex === configLength - 1) {
            // Right edge.
            if (innerColIndex === 0) {
              airplaneSeatType = "A";
            } else if (innerColIndex === colLength - 1) {
              airplaneSeatType = "W";
            } else {
              airplaneSeatType = "M";
            }
          } else {
            // Middle.
            if (innerColIndex === 0 || innerColIndex === colLength - 1) {
              airplaneSeatType = "A";
            } else {
              airplaneSeatType = "M";
            }
          }

          seats[`${colIndex}_${innerColIndex}_${rowIndex}`] = airplaneSeatType;
        }
      }
    }
  }

  return seats;
}

function assignSeats(
  airplaneSeats,
  passengerCount,
  seatPriority = ["A", "W", "M"]
) {
  let seatAssignmentMap = {};
  let seatOrder = [];

  //  iterate over the priority
  seatPriority.forEach((priority) => {
    // filter seats by current priority
    let seatsByCurrentPriority = Object.entries(airplaneSeats).filter(
      (item) => item[1] === priority
    );

    // concatenate the seats returned from the above filter
    seatOrder = seatOrder.concat(seatsByCurrentPriority);
  });

  // create a new map by concatenating with a key and value pair
  seatOrder.slice(0, passengerCount).forEach((item, index) => {
    let key = item[0];

    seatAssignmentMap[key] = {
      airplaneSeatType: airplaneSeats[key],
      passengerNumber: index,
    };
  });

  // seat assignement map  will have object like structure like 0_1_0:{airplaneSeatType: "W", passengerNumber:1}
  return seatAssignmentMap;
}

function print(airplaneSeatConfig, seatAssignmentMap) {
  // extract the rows
  let rows = airplaneSeatConfig.map((config) => config[1]);

  //  iterate over the rows
  let rowsLength = rows?.length;

  let configLength = airplaneSeatConfig?.length;

  for (let rowIndex = 0; rowIndex < rowsLength; rowIndex++) {
    let row = [];

    // grab the airplane seat config and iterate over that ... we can access all the arrays
    for (let colIndex = 0; colIndex < configLength; colIndex++) {
      // grab the subarray
      const columnSeatMap = airplaneSeatConfig[colIndex];

      // grab the specfic array column
      const columns = columnSeatMap[0];

      const colLength = columns;

      // check if rowIndex under the constratin of the row number of that array
      if (rowIndex < airplaneSeatConfig[colIndex][1]) {
        // iterate over the columns
        for (
          let innerColIndex = 0;
          innerColIndex < colLength;
          innerColIndex++
        ) {
          // grab the key so that we can access the values from seat assignment map
          const airplaneSeatKey = `${colIndex}_${innerColIndex}_${rowIndex}`;

          const airplaneSeat = seatAssignmentMap[airplaneSeatKey];

          //  if there is a value
          if (airplaneSeat) {
            // push to row with color code
            row.push(
              colorMap[airplaneSeat.airplaneSeatType](
                (airplaneSeat.passengerNumber + 1).toString().padStart(2)
              )
            );
          } else {
            row.push(blue("■".padStart(2)));
          }
        }
        row.push("    ");
      } else {
        // console.log("——|".repeat(columns))
        row.push("——|".repeat(columns));
        row.push("    ");
      }
    }

    console.log(
      row
        .map((r) => {
          if (r.endsWith("|")) {
            return r.slice(0, r.length - 1);
          }
          return r;
        })
        .join("|")
    );
  }

  //   console log the results
}

// grab the input from the process
const args = process.argv;

if (args.length != 4) {
  console.error(
    "Please insert the airplane seat arrangement and the passenger count correctly"
  );
  process.exit(1);
}

try {
  // parse the airplane seat arrangement
  const aireplaneSeatConfig = JSON.parse(args[2]);

  //   accept passenger count as an int
  const passengerCount = parseInt(args[3]);

  if (passengerCount < 0 && passengerCount > 100) {
    console.error("passenger count needs to be lessthan 100 and greaterthan 1");
    process.exit(1);
  }

  const airplaneSeats = buildAirplaneSeatLayout(aireplaneSeatConfig);
  const seatAssignmentMap = assignSeats(airplaneSeats, passengerCount);
  print(aireplaneSeatConfig, seatAssignmentMap);
} catch (err) {
  console.log("something went wrong");
  console.log("err is ", err);
}
