# Airplane Seat Arrangement

## Use the below command to run

```
node index.js <airplaneSeats> <passengersCount>

```

## Sample input

```
 node index.js "[[3,2],[4,3],[2,3],[3,4]]" "30"

```
